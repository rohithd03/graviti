import './App.css';
import { useJsApiLoader, GoogleMap, Marker, DirectionsRenderer } from '@react-google-maps/api';
import { useState } from 'react';

// const api_key = process.env.API_KEY;

const center = {lat: 17.3616, lng: 78.4747}
const libs = ['places'];

function App() {

  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: process.env.REACT_APP_API_KEY,
    libraries: libs
  })

  const [origin, setOrigin] = useState("");
  const [destination, setDestination] = useState("");
  const [distance, setDistance] = useState("");
  const [directionsResponse, setDirectionsResponse] = useState(null);


  if(!isLoaded){
    return <div>Google maps not Loaded yet!</div>
  }

  async function calculate(){
    // eslint-disable-next-line no-undef
    const directionsService = new google.maps.DirectionsService();
    const results = await directionsService.route({
      origin: origin,
      destination: destination,
      // eslint-disable-next-line no-undef
      travelMode: google.maps.TravelMode.DRIVING
    })
    setDirectionsResponse(results);
    setDistance(results.routes[0].legs[0].distance.text)
    console.log("Results: ", results)

  }



  return (
    <div className="App">
      <header className="App-header">
        <img src={"/graviti.png"} className="logo" alt="logo" />
      </header>
      <div className='title'>
        <div className='title-content'>
          Let's calcuate <strong>distance</strong> from Google maps
        </div>
      </div>
      <div className='container'>
        <div className='leftpane'>
          <div className='form'>
            <div>
              <label>Origin</label>
              <input type="text" value={origin} onChange={(e) => {setOrigin(e.target.value)}} />
            </div>
            <div className='btn'>
              <button onClick={calculate}>Calculate</button>
            </div>
            <div>
              <label>Destination</label>
              <input type="text" value={destination} onChange={(e) => {setDestination(e.target.value)}} />
            </div>
          </div>
          <div className='result'>
            <div className='top'>
              <div className='lab'>
                Distance
              </div>
              <div className='kms'>
                {distance}
              </div>
            </div>
            {directionsResponse && <div className='distance'>
              <p>The distance between <strong>{origin}</strong> and <strong>{destination}</strong> is <strong>{distance}</strong>.</p>
            </div>}
          </div>
        </div>
        <div className='rightpane'>
            <GoogleMap center={center} zoom={15} mapContainerStyle={{width: '100%', height: '100%'}} options={{
              zoomControl: false,
              streetViewControl: false,
              mapTypeControl: false,
              fullscreenControl: false
            }}>
              <Marker position={center} />
              {directionsResponse && <DirectionsRenderer directions={directionsResponse} />}
            </GoogleMap>
        </div>
      </div>
    </div>
  );
}

export default App;
