# Google Maps

### `setup`
Add an empty .env file with contents
```
REACT_APP_API_KEY = "Your key"
```

### `npm init`

Installs all the dependent libraries

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `Images`
![NewPage](/images/Newpage.png)